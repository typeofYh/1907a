/*
 * @Author: Yh
 * @LastEditors: Yh
 */

 class Dep {  // 队列存储所有的watcher 
   constructor(){
     this.watchers = [];
   }
   addWatcher(watcher){
     this.watchers.push(watcher);
   }
   notify(){
     this.watchers.map(item => {
        // item 每一个watcher
        item.update();
     })
   }
 }
 // 监听者
 class Watcher {
    constructor(vm, key, cb){
      // vm 实例
      // key 属性名
      // cb 监听属性值变化的回调函数
      this.vm = vm;
      this.key = key;
      this.cb = cb;

      Dep.target = this;  // 当前watcher
      // 获取属性 触发get函数
      eval(`this.vm.${key}`) // get 把当前watcher添加到dep中
      Dep.target = null;
    }
    update(){ // 属性更新
      const val = eval(`this.vm.${this.key}`);
      this.cb.call(this.vm, val)
    }
 }

 /**
  * [Compile 解析指令和文本]
  */
 class Compile {
   constructor($el, vm){
     this.$el = document.querySelector($el);
     this.vm = vm; // myVue 的实例
     // 文本（节点中文本） 和 指令（属性节点）
     this.compile(this.$el);
   }
   compile($el){
     let childs = Array.from($el.childNodes)
    //  console.log($el,childs, $el.attributes);
     // 过滤掉换行了
     childs = childs.filter(el => !(el.nodeName === '#text' && /^\n\s+$/.test(el.nodeValue)));  // 找到el所有的子元素
     $el.attributes.length && (childs = childs.concat([...$el.attributes]))
     childs.map(els => {
        // console.log(els.nodeType);
        switch(els.nodeType){
          case 1: // nodeType 是元素节点 元素节点有子元素
            this.compile(els); // 找子元素
          break;
          case 2:  // 属性节点  v-开始  @开始
            if(/^(v-|@)/.test(els.nodeName)){
              console.log('属性节点', els, els.nodeName, els.nodeValue);
            }
          break;
          case 3:   // 文本节点  {{内容}}
            const reg = /\{\{([^\}]+)\}\}/g;
            if(reg.test(els.nodeValue)){
              this.updateText(els, els.nodeValue.match(reg), els.nodeValue); 
            }
          break;
        }
     })
   }
   updateText(el, key, text){ // 更新文本节点视图
    let curVal = text; // text 是原始文本内容 携带{{}}
    let watcherVal = text;
    if(Array.isArray(key)){
      key.map(item => {
        const keys = item.slice(2, -2); // 获取了vm.keys
        curVal = curVal.replace(item, eval(`this.vm.${keys}`)); // 初始替换文本内容
        el.nodeValue = curVal;
        // 当获取属性时要设置监听者
        new Watcher(this.vm, keys, (data) => {  // 只要数据变化 触发当前函数
          curVal = watcherVal.replace('{{'+keys+'}}', data);
          // watcherVal = curVal;
          // 修改el内容
          el.nodeValue = curVal;
        })
      })
    }
   }
 }

 class MyVue {
   constructor({el,data}){
      this.$data = data;
      this.$el = el;
      this.oDep = new Dep();
      this.obServer(this.$data); 
      // 指令解析
      this._compile = new Compile(this.$el, this);
   }
   /**
    * [obServer 实现数据劫持]
    * 需要把$data所有的属性变成访问器属性
    * Object.defineProperty
    * @return  {[type]}  [return description]
    */
   obServer(data){
      if(!(typeof data === 'object')){
        return;
      }
      Object.keys(data).map(key => {
        // console.log(key, data[key]);
        let value = data[key];
        this.observerKey(data, key, value);
        // $data 属性挂载到组件实例上
        this.proxyData(key);
      })
   }
   observerKey(data, key ,value){
      this.obServer(value);
      Object.defineProperty(data, key, {
        get:() => {
          // console.log('get---------',key, Dep.target);
          // 监听属性获取 添加watcher监听者，方便监听属性修改
          Dep.target && this.oDep.addWatcher(Dep.target)
          return value;
        },
        set: (val) => {
          // 监听属性修改
          if(val !== value){  // 比较属性值变化 
            // 触发watcher 驱动视图变化
            value = val;
            this.oDep.notify(); // 触发wather
          }
        }
      })
   }
   proxyData(key){ // $data 属性放到this上 可以直接通过this修改data属性值
      // this 是组件实例
      this.$data[key] && Object.defineProperty(this, key, {
        get(){
          return this.$data[key];
        },
        set(val){
          // console.log('set---proxy:', key ,val);
          this.$data[key] = val;
        }
      })
   }
 }