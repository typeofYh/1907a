/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const redux = require('./myredux')
const ADD = 'ADD';

const reducer = (state = {count: 1}, action) => {
  console.log('reducer：', state, action);
  switch(action.type){
    case ADD:
      return {
        ...state,
        count: state.count + 1
      }
    default :  // 返回仓库默认状态
      return state;
  }
}

// 创建仓库
const store = redux.createStore(reducer)  // 默认触发一次dispatch

store.subscribe(() => {  // 监听仓库变化  发布订阅
  console.log('state-change----')
})


console.log('getState:',store.getState()); // 获取仓库最新状态

// 修改仓库状态 派发 reducer ， reducer返回仓库最新状态
store.dispatch({
  type: ADD
})

console.log('getState:',store.getState()); // 获取仓库最新状态

// 修改仓库状态 派发 reducer ， reducer返回仓库最新状态
store.dispatch({
  type: ADD
})
