import { createStore } from "redux";
import { CAR_ADD } from "./actionType";
const reducer = (
  state = {
    car: {
      total: 0,
      datalist: [
        {
          name: "辣条",
          price: 10,
          shopid: 1
        }
      ]
    }
  },
  { type, payload }
) => {
  switch (type) {
    case CAR_ADD:
      const curShopItem = state.car.datalist.find(
        ({ shopid }) => payload.shopid === shopid
      );
      curShopItem.count = (curShopItem.count || 0) + 1;
      return {
        ...state
      };
    default:
      return state;
  }
};

const store = createStore(reducer);

export default store;
