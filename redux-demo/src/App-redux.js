/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import "./styles.css";
import store from "./store";
import { CAR_ADD } from "./store/actionType";
import { useDispatch, useSelector } from "./lib/my-react-redux";

export default function App() {
  const dispatch = useDispatch(); // 首先拿到store
  const { car } = useSelector((state) => {
    console.log("useSelector---", state);
    return state;
  }); // 获取仓库状态的
  // const { car } = { car: { datalist: [], total: 0 } };

  return (
    <div className="App">
      <h1>----------------app-react-redux-------</h1>
      {car.datalist.map((item) => (
        <div key={item.shopid}>
          {item.name} ---- ¥{item.price} ---- count:{item.count || 0}
          <button
            onClick={() => {
              dispatch({
                type: CAR_ADD,
                payload: {
                  shopid: item.shopid
                }
              });
            }}
          >
            +
          </button>
        </div>
      ))}

      <div>总价{store.getState().car.total}</div>
    </div>
  );
}
