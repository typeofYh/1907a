/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { createContext, useContext, useState } from "react";
const context = createContext();

export const Provider = ({ store, children }) => {
  // console.log(store, context);
  return <context.Provider value={{ store }}>{children}</context.Provider>;
};

export const useDispatch = () => {
  const value = useContext(context);
  console.log(value);
  return value.store.dispatch;
};

export const useSelector = (handler) => {
  const value = useContext(context);
  const [state, setState] = useState(value.store.getState());
  value.store.subscribe(() => {
    console.log("change");
    setState(handler(value.store.getState()));
  });
  return state;
};

export const connect = () => {};
