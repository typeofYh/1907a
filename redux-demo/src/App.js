/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import "./styles.css";
import store from "./store";
import { CAR_ADD } from "./store/actionType";
import { useState } from "react";

export default function App() {
  const [test, setTest] = useState();
  let unSubscribe = store.subscribe(() => {
    console.log("仓库状态变化", store.getState());
    setTest({}); // app 渲染视图
    unSubscribe();
  });
  console.log("render");
  return (
    <div className="App">
      {store.getState().car.datalist.map((item) => (
        <div key={item.shopid}>
          {item.name} ---- ¥{item.price} ---- count:{item.count || 0}
          <button
            onClick={() => {
              store.dispatch({
                type: CAR_ADD,
                payload: {
                  shopid: item.shopid
                }
              });
            }}
          >
            +
          </button>
        </div>
      ))}

      <div>总价{store.getState().car.total}</div>
    </div>
  );
}
