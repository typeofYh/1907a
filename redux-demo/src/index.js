/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import App from "./App";
import AppRedux from "./App-redux";
import store from "./store";
import { Provider } from "./lib/my-react-redux";
import React from "react";
import { render } from "react-dom";

render(
  <Provider store={store}>
    <App />
    <AppRedux />
  </Provider>,
  document.getElementById("root")
);
