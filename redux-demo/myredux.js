/*
 * @Author: Yh
 * @LastEditors: Yh
 */
function isPlainObject(obj) {
  if (typeof obj !== 'object' || obj === null) return false

  let proto = obj
  while (Object.getPrototypeOf(proto) !== null) {
    proto = Object.getPrototypeOf(proto)
  }

  return Object.getPrototypeOf(obj) === proto
}


const randomString = () => Math.random().toString(36).substring(7).split('').join('.');

const ActionTypes = {
 INIT: `@@redux/INIT${/* #__PURE__ */ randomString()}`,
 REPLACE: `@@redux/REPLACE${/* #__PURE__ */ randomString()}`,
 PROBE_UNKNOWN_ACTION: () => `@@redux/PROBE_UNKNOWN_ACTION${randomString()}`
}



const createStore = (reducer, preloadedState) => {

  let currentState = preloadedState;
  let currentReducer = reducer;
  let currentListeners = [];
  let nextListeners = currentListeners;

  const ensureCanMutateNextListeners = () => {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice()
    }
  }

  const dispatch = (action) => { // 函数 作用派发reducer
    // action 必须时对象
    if (!isPlainObject(action)) {
      throw new Error(
        `Actions must be plain objects. Instead, the actual type was: '${action}'. You may need to add middleware to your store setup to handle dispatching other values, such as 'redux-thunk' to handle dispatching functions. See https://redux.js.org/tutorials/fundamentals/part-4-store#middleware and https://redux.js.org/tutorials/fundamentals/part-6-async-logic#using-the-redux-thunk-middleware for examples.`
      )
    }
    // action 必须有type属性
    if (typeof action.type === 'undefined') {
      throw new Error(
        'Actions may not have an undefined "type" property. You may have misspelled an action type string constant.'
      )
    }
    // 触发reducer 拿仓库最新状态
    currentState = currentReducer(currentState, action)
    // 通知subscribe 触发订阅函数
    const listeners = (currentListeners = nextListeners)
    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i]
      listener()
    }
    
    return action;
  }

  const subscribe = (handler) => { // 作用订阅仓库状态变化
    // handler 监听函数 
    // subscribe 调用一次添加一个函数，最终仓库变化  要通知所有的handler
    if (typeof handler !== 'function') {
      throw new Error(
        `Expected the listener to be a function. Instead, received: '${kindOf(
          listener
        )}'`
      )
    }
    let isSubscribed = true
    ensureCanMutateNextListeners();
    // 把handler放入到队列
    nextListeners.push(handler);

    return function unsubscribe() {
      if (!isSubscribed) {
        return
      }

      isSubscribed = false

      ensureCanMutateNextListeners()
      const index = nextListeners.indexOf(handler)
      nextListeners.splice(index, 1)
      currentListeners = null
    }
  }

  const getState  = () => {  // 获取仓库状态
    return currentState
  }
  // 默认调用了一次dispatch
  dispatch({ type: ActionTypes.REPLACE }) 

  const store = {
    dispatch,
    subscribe,
    getState
  }
  return store;
}


module.exports = {  
  createStore
}