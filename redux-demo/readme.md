<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
### redux
- 是一个状态的管理的库
维护单向数据流（从一个方向来得数据，只能在同一个地方修改）

#### 整个redux管理流程
通过`createStore`创建仓库，得到仓库实例，通过实例的`dispatch`方法派发`action`触发`reducer`,`reducer`函数返回仓库最新状态

#### redux三大设计原则
1. 单一数据源
2. State 是只读的
3. 使用(reducer)纯函数来执行修改

```js

const flag = true;

function func(flag){
  if(flag){
    return {
      a:1
    }
  }
}

func(flag)
```


#### redux怎么跟react进行关联使用的？
