/*
 * @Author: Yh
 * @LastEditors: Yh
 */
// js解析分为两个阶段 
// 1. 编译阶段： 确定作用域
// 2. 执行阶段： 确定上下文
/** 
console.log('start');  // 同步  1
const run = (time) => { // 确定函数作用域
  console.log('run'); // 同步 4
  setTimeout(() => { // 异步操作 5 放入到事件队列 在往下走
    // 通关之后得到key
    const key = Math.random().toString().slice(2,5);
    console.log(key);
    console.log(1)
  },time * 1000)
  console.log(2) // 同步 6
}
console.log(5); // 同步 2
run(3) // 同步 3 开始执行函数
console.log('end');  // 同步 7 所有同步执行完成 查找事件队列
// js执行顺序从上到下执行，碰到异步操作不会等待异步操作执行完成，会先把异步操作放到事件队列中，直接执行下面的同步操作，所有的同步操作执行完成
// 去事件队列中查找异步任务执行
*/
// 异步操作 定时器、事件、（ajax、fetch）发请求


/** 
const run  = (time, ck) => {
  setTimeout(() => { 
    const key = Math.random().toString().slice(2,5);
    ck(key);
  },time * 1000)
}
let count = 0;
// 回调函数
// 没有promise之前获取 异步运行结果采用回调函数的形式
run(3, function(key){
  console.log('通关了：', ++count,'密钥：', key, '可以开始下一关');
  run(2, function(key){
    console.log('通关了：', ++count,'密钥：', key, '您当前是倔强青铜，可以开始下一关');
    run(5, function(key){
      console.log('通关了：', ++count,'密钥：', key, '您当前是白银，可以开始下一关');
    })
  })
}) 
*/

const run  = (time) => {
  console.log('run------',time);
  return new Promise((resolve,reject) => { // 同步操作  包裹异步操作的容器
    // promise 有三种状态 padding resolve reject
    // console.log(4); // 同步 3
    setTimeout(() => { // 异步任务 事件队列等待执行
      const key = Math.random().toString().slice(2,5);
      // 拿到key
      // 调用resolve执行的then方法的第一个参数
      // 调用reject执行的then方法的第二个参数
      Math.random() > 0.1 ? resolve(key) : reject('通过失败') 
    },time * 1000)
    // console.log(3); // 同步 4
  })
}
// console.log('start');  //同步 1

const game = [
  {
    id:0,
    leval: '',
    time: 3
  },
  {
    id:1,
    leval: '倔强青铜',
    time: 2
  },
  {
    id:2,
    leval: '白银',
    time: 5
  }
]
let count = 0;
const starGmme = async () => {
  try {
    // const key = await run(3) 
    // console.log('通关了：', count++,'密钥：', key, '可以开始下一关')
    // const key1 = await run(2) 
    // console.log('通关了：', count++,'密钥：', key1, '您当前是倔强青铜，可以开始下一关')
    // const key2 = await run(5) 
    // console.log('通关了：', count++,'密钥：', key2, '您当前是白银，可以开始下一关')
    for(let i=0;i<game.length;i++){
      const key = await run(game[i].time);
      console.log('通关了：', count++,'密钥：', key, '您当前是'+game[i].leval+'可以开始下一关')
    }
  }catch (error){
    console.log(count + ' : ' + error);
  }
}
// starGmme();

// async 在声明的时候在函数前加async 关键字，在函数根作用域中可以使用await 关键字，调用跟普通函数一样

/**
// 第一关
run(3)  
.then((key) => { // 异步 调用resolve的时候执行
  console.log('通关了：', count++,'密钥：', key, '可以开始下一关')
  return run(2); 
}, (error) => {
  console.log('第一关错误', error);
  // return '错误key'
}) 
.then((key) => { // 第二关
  console.log('通关了：', count++,'密钥：', key, '您当前是倔强青铜，可以开始下一关');
  return run(5);
})
.then((key) => { // 第三关
  console.log('通关了：', count++,'密钥：', key, '您当前是白银，可以开始下一关');
})
.catch(error => {
  console.log(count + ' : ' + error);
})

console.log('end'); // 同步 5

 */

 // generator 在声明函数时在函数前加*号, 调用跟普通函数一样， 在函数内可以使用yield关键字
 // 控制器 

 const a = function *(){
  console.log('-------a');
  const key = yield run(1)
  console.log('-------b', key);
  const key1 = yield run(2)
  console.log('-------ddddd', key1);
  yield console.log(22222);
  console.log('-------c');
  return "success-----res";
 }

 const _a = a(); // 调用a函数

 // _a 执行器 遍历器
// _a.next().value.then(res => {
//   _a.next(res) // 2
// }, (error) => {
//   console.log(error);
// })
// // 
// //  _a.next() // 3

//  console.log('_a',_a);

function autoGenerator(oGenerator){
  return new Promise((resolve, reject) => {
    const run = (nextValue) => {
      const {value, done} = oGenerator.next(nextValue);
      if( done ){
        resolve(value);
        return;
      }
      if(value instanceof Promise){
        value.then(res => {
          run(res);
        }, (error) => {
          reject(error);
        })
        return;
      }
      run(value);
    }
    run()
  })
}
autoGenerator(_a).then(res => {
  console.log(res);
})

 // generator函数有啥好处，在外部可以控制函数内部脚本执行顺序，控制函数内部状态
 // async 函数是generator函数的语法糖