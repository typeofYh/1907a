/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const http = require('http');

const server = http.createServer((req, res) => {
  console.log(req.url);
  if(req.url === '/api/list'){
    // res是响应对象
    // 在响应的时候设置headers头
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8100');
    res.end(JSON.stringify({
      code:1,
      data: [
        {
          name:'zs',
          id:1
        }
      ]
    }))
  }
})

server.listen(3000, () => {
  console.log('port is 3000')
})