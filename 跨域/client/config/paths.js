/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const path = require('path');
const resolveApp = (dirname) => path.join(__dirname,'../', dirname);
module.exports = {
  resolveApp,
  app: resolveApp('./'),
  appSrc: resolveApp('./src'),
  appPublic: resolveApp('./public'),
}
