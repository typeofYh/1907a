/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const path = require('path');
const { appSrc, resolveApp } = require('./config/paths');
const HTMLWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  mode: 'development',
  entry: path.join(appSrc, 'index.js'),
  output: {
    filename: '[name].[hash:5].bundle.js',
    path: resolveApp('dist')
  },
  plugins: [
    new HTMLWebpackPlugin({
      title: '我的页面',
      template: resolveApp('./public/index.html')
    })
  ],
  devServer: {
    port: 8100
  }
}