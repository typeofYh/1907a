/*
 * @Author: Yh
 * @LastEditors: Yh
 */
// 一进页面发请求

const app = document.getElementById('app');

fetch('http://localhost:3000/api/list', {
  method: 'POST'
}).then(res => {
  return res.json();
}).then(data => {
  console.log(data);
})


const baiduBaseUrl = 'https://www.baidu.com/sugrec';
const searchText = document.getElementById('seachText');
const searchlist = document.querySelector('.searchlist');

const formatUrl = (baseUrl, params = {}) => {
  return `${baseUrl}?${Object.keys(params).map(key => `${key}=${params[key]}`).join('&')}`
}
const defaultCb = "test_"+Math.random().toString().slice(3)
const Jsonp = (url, option = {params:{cb:defaultCb}}) => {
  url = formatUrl(url, option.params)
  return new Promise((resolve, reject) => {
    const scriptTag = document.createElement('script');
    scriptTag.src = url;
    // 要定义函数
    const cbName = option.params.cb;
    window[cbName] = (data) => { // 加载js脚本后端调用的
      resolve(data);
    }
    scriptTag.onerror = reject;
    document.body.append(scriptTag); // 加载js脚本了
  })
}

searchText.addEventListener('input', async () => {
  const { g } = await Jsonp(baiduBaseUrl, {
    params: {
      pre:1,
      p:3,
      ie:'utf-8',
      json:1,
      prod:'pc',
      from:'pc_web',
      sugsid:'36425,36367,34813,36165,34584,35978,36055,35802,36232,26350',
      wd:searchText.value,
      req:2,
      csor:4,
      cb:'test', // 回调函数名称
      _:Date.now()  // 时间戳
    }
  })
  searchlist.innerHTML = g.map(item => {
    return `<li>${item.q}</li>`
  }).join('')
})