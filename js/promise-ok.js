/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const getOk = (time) => new Promise((resolve) => {
  setTimeout(() => {
    resolve('ok')
  }, time)
})

// getOk(10000).then(res => {
//   console.log(res);
// })



// [5,[[4,3],2,1]]
// (5 - 4) - ((4 - 3) - 2 - 1);


function reduceNum(arr) {
  return arr.reduce((val, item) => {
    if(Array.isArray(val)){
      val = reduceNum(val);
    }
    if(Array.isArray(item)){
      item = reduceNum(item);
    }
    return val - item;
  })
}

// console.log(reduceNum([5,[[4,3],2,1]]));


Array.prototype.myReduce = function(ck, defultValue){
  let startIndex = 0;
  let val = defultValue;
  if(defultValue === undefined){
    startIndex = 1;
    val = this[0];
  }

  for(;startIndex<this.length;startIndex++){
    // console.log(startIndex);
    val = ck(val, this[startIndex], startIndex, this)
  }
  return val;
}

const arr = [1,2,3,4];  // arr是数组实例
const res = arr.myReduce((val, item, index, arr) => {
  // val 是0
  // val 是函数返回值
  console.log(val, item, index, arr)
  return val  + item;
}, 10)
console.log(res);