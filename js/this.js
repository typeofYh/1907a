/*
 * @Author: Yh
 * @LastEditors: Yh
 */
// 'use strict';  // 严格模式


let componentConfig = {
  data(){
    console.log("data函数中的this:",this);  // componentConfig
    return {
      selectData: {}
    }
  },
  watch: {
    selectData(){
      console.log('watch', this);
      const run1 = () => {
        console.log('run1',this);
      }
      run1();
      const run2 = function() {
        console.log('run2',this);
      }
      run2();
    }
  },
  methods: {
    setSelectData(){
      console.log('setSelectData:', this); // componentConfig
      this.selectData = {  // 修改componentConfig对象下的selectData
        name: 'zs'
      }
    },
    setSelect: function(){
      console.log('setSelect:',this); // methods对象
      this.setSelectData.call(componentConfig); // 隐式调用
      // 通过 call apply 方法进行调用
      // this指向 第一个参数
    }
  }
}

componentConfig.watch.selectData();

// const componentData = componentConfig.data(); // 调用data
// componentConfig = {  //把data的返回值 对象中的每一个属性放到 组件的大对象中
//   ...componentConfig,
//   ...componentData
// }

// console.log(componentConfig);
// componentConfig对象下的data方法调用的
// 方法调用： this指向指向拥有该方法的对象

// const rundata = componentConfig.data;  // 取函数 存到rundata这个变量中 rundata就是一个普通函数
// rundata(); // 普通函数调用
// this指向（全局对象） 严格模式：this指向undefined

// const a = function(){
//   console.log(this);
// }
// a();  // 普通函数调用
// this指向（全局对象） 严格模式：this指向undefined

// componentConfig.methods.setSelect();  // 方法调用


// const obj = {
//   oname: 'obj',
//   age:10,
//   setName(name, age){
//     this.oname = name;
//     this.age = age;
//   }
// }

// const run = function(name,age){
//   this.setName(name,age);
// }
 
// run.call(obj, '_call', 11);  

// run.apply(obj, ['_apply', 12]); 

// const runbind = run.bind(obj, '_bind', 14); // 绑定函数的this指向 不会调用函数返回修改this指向之后的函数

// window.setName = function(name,age){
//   this.oname = name;
//   this.age = age;
// }

// runbind.call(window,'_window',16);  // 只要通过bind修改this指向之后的函数，this指向不会改变了



// function Dialog (){
//   console.log('dialog',this);
//   window.a1 = this;
// }

// Dialog();  // 普通函数调用
// const a = new Dialog();  // new 关键字调用

