<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
### js


#### this

1. 当前js运行环境的上下文对象，this也是关键字
2. this是运行时形成，函数调用时形成

this规律
1. 方法调用： this指向指向拥有该方法的对象
2. 普通函数调用：this指向（全局对象） 严格模式：this指向undefined
3. 隐式调用：通过 call和apply 进行调用，this指向call和apply的第一参数 
4. 通过bind修改this指向，返回绑定this指向之后的函数，只要通过bind修改this指向之后的函数，this指向不会改变了
5. 通过new 关键字调用的函数叫(构造函数)：this指向实例对象,函数返回值就是实例对象
  5.1 通过new 关键字调用的函数干了4件事：
    - 生成一个新的实例对象
    - 运行该函数
    - 修改该函数的this指向
    - 返回该函数的this指向
  
6. 箭头函数：this不会改变，this在定义时形成不是调用时形成



修改this指向
1. call
2. apply
3. bind



### 基础类型 存在栈内存中
string number boolean null undefined symbol bigint

### 引用类型值
Object 存储在堆内容，主要因为无法确定该对象的内存大小，如果存在栈内存中无法计算

const a = {name:"a"}
const b = a;
b.name = "b";


### 怎么判断当前对象的类型？
判断简单类型值通过typeof 
判断对象的构造函数 XXX instanceof 构造函数
```js
(val) => Object.prototype.toString.call(val)
```


### 什么是原型链
js中一切皆对象
1. 实例对象上有__proto__
2. 函数对象有prototype属性
以上两个找的都是原型对象
实例对象的__proto__属性一直往下找，直到找到null的这样的一条链叫原型链
节省内存开销，把公共方法和属性放到原型上，每次实例对象的时候先找自身属性和方法，
找不到接着找原型属性和方法，直到找到null



## 什么是浅拷贝? 只拷贝对象第一层的值，如果子元素还有引用类型不会进行拷贝
```js
const a = {}
const b = {...a}

const a = {}
const b = Object.assign({},a)
```

## 什么是深拷贝 对象下所有的值都会重新创建堆内存地址
```js
const a = {}
const b = JSON.parse(JSON.stringify(a))  过滤方法 时间日期对象 正则
```
通过递归
```JS
function initCloneObject(object) {
  return (typeof object.constructor === 'function')
    ? Object.create(Object.getPrototypeOf(object))
    : {}
}
const cloneArr = (arr) => {
    return arr.map(item => {
        return cloneDeep(item);
    })
}
const baseClone = (val) => {
    const newObj = {};
    for(let key in val){
        newObj[key] = cloneDeep(val[key])
    }
    return newObj;
}
const cloneDeep = (val) => {
    if(typeof val !== 'object'){
        return val;
    }
    // val肯定是一个对象
    if(Array.isArray(val)){  // 数组
        return cloneArr(val)
    }
    // val是一个函数的时候
    if(typeof val === 'function'){
        return initCloneObject(val)
    }
    // 
    return baseClone(val)
}
const a = {
    name:"a",
    info: {
        age:10
    },
    say(){
        
    },
    list:["a",{name:"b"}]
}
const b = cloneDeep(a)
console.log(b);
```



### 防抖 节流 必须背下来！！！


### promise / 异步任务 / eventloop / async await

防抖 节流 封装promise抄两遍

setTimeout(() => {   放到eventloop 等待执行  4
  console.log(1)
})

new Promise((resolve) => {
  console.log(4)  // 同步  1
  resolve('6')
}).then((res) => {    // 异步 微任务 等待执行  3
  console.log(res)
})

console.log(2) // 同步 2  所有同步执行完成 找异步任务 