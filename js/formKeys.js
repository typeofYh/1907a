/*
 * @Author: Yh
 * @LastEditors: Yh
 */

const addKey = (resObj, value, key) => {
  let newKey = key; // a
  let targetValue = value; // {b: 1,c: 2,d: {e: 5}}
  if(Array.isArray(value)){ // 数组
    targetValue = value;
  } else { // 对象
    targetValue = Object.keys(value); //[b,c,d]
  }
  targetValue.map((itemKey, i) => { 
    // itemKey 有两种情况，一种遍历的是数组，第二种遍历的对象的属性名
    const itemVal = Array.isArray(value) ? itemKey : value[itemKey];
    newKey = Array.isArray(value) ? `${key}[${i}]` : `${key}.${itemKey}`;
    if(typeof itemVal === 'object'){
      addKey(resObj, itemVal, newKey);
    } else {
      resObj[newKey] = itemVal;
    }
  })
}

function flatten(obj) {
  // 你的代码
  return Object.keys(obj).reduce((val, key) => {
    let value = obj[key]; // {b: 1,c: 2,d: {e: 5}}
    // key a , value {b: 1,c: 2,d: {e: 5}}
    // a.b  a.c   a.d.e  
    if(typeof value === 'object'){
      addKey(val, value, key)  // a b
    } else {
      val[key] = value; // c: 3
    }
    return val;
  }, {})
}





// 最终效果是执行

console.log(flatten({

  a: {

    b: 1,

    c: 2,

    d: {e: 5}

  },

  b: [1, 3, {a: 2, b: 3}],

  c: 3,
  d: [{
    c: 'd-c',
    d: {
      e: 'd-d-e'
    }
  }]

})); 



// 得到

