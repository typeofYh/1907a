<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
### git 
分布式版本管理工具

### svn
集中式版本管理工具

### git 安装
1. mac 电脑自带git
2. window 电脑[下载地址](https://www.git-scm.com/download/)

### git 初始配置

- 查看配置
```shell
git config --list
```
- 必须配置
```shell
git config --global user.email "yourEmail"
```
```shell
git config --global user.name "yihang"
```
### 查看远程配置

```shell
git remote -v
```

### 本地仓库跟远程仓库进行关联
```shell
git remote add origin '远程仓库地址'
```

1. 本地初始化仓库(创建.git文件夹)
```shell
git init
```

2. 首先在远程仓库创建好新项目通过`git clone`克隆，（克隆下的仓库自动会跟远程仓库进行关联）
```
git clone 'origin url'
```


### ssh key
1. 首页在本地生成密钥
```shell
ssh-keygen
```
找到`.ssh`目录下`id_rsa.pub`文件


### 分支

> 什么是分支？

- HEAD 指令，方便多人写协作，相当于两个工作线

```shell
git branch #查看本地分支
git branch -a #查看所有分支
git barnch -r #查看远程分支
git branch barnchname #创建分支
git checkout branchname #切换分支
git checkout -b branchname #切换分支，并创建分支
git checkout -b branchname origin/branchname #切换并创建分支并关联远程分支
git pull origin 主分支 --allow-unrelated-histories #初始拉取主分支的内容
```

> 分支开发流程

- 进公司克隆仓库，克隆下来是主分支内容
```
git clone '仓库地址'
```
- 分支命名规则
```js
main、master // gitlab主分支  受保护分支 
developer // 总开发分支
dev_yh_3.12_git // dev 开发环境 姓名缩写 分支创建日期 需求主要内容
dev_a_3.12_home
dev_b_3.12_detail
dev_c_3.12_list
// 每天敲代码第一件事拉取developer分支代码
// 每天结束工作之后主动合并到developer
// 最终项目上线 组长把developer分支合到main分支


test // 测试
test_yh_
production // 生产环境
```

### 解决冲突

1. 拉取最新代码
2. 手动解决冲突（保留当前修改｜保留传入修改｜保留所有修改｜比较修改）
3. 在次提交，推送最近代码

### 查看文件状态
```
git status
```

### 文件回滚
在文件回滚之前保存文件状态
```
git reset --hard commitid
```
会产生新的提交历史记录
```
git revert commitid
```

### 文件暂存
```
git stash
```
### 恢复最近一次暂存
```
git stash pop
```
### 查看暂存列表
```
git stash list
```

### sourcetree 可视化工具