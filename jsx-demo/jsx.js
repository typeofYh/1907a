/*
 * @Author: Yh
 * @LastEditors: Yh
 */
// const div = <div>
//   <p>hello jsx!</p>
// </div>

// console.log(div);


"use strict";

const div = /*#__PURE__*/ React.createElement(
  "div",
  null,
  /*#__PURE__*/ React.createElement(
    "p",
    {
      className: "title"
    },
    "hello jsx!"
  ),
  /*#__PURE__*/ React.createElement(
    "span",
    null,
    /*#__PURE__*/ React.createElement("i", {
      className: "icon"
    }),
    /*#__PURE__*/ React.createElement("b", null, "icon")
  )
); // jsx语法
// jsx就是React.createElement的语法糖
// babel根据语法预设进行解析转义，转义成最终浏览器可解析的语法

console.log(div);