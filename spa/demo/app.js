/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const http = require('http');
const fs = require('fs');
const path = require('path');

const publicPath = (filename) => path.join(__dirname, './public', filename);
const paths = ['/','/type','/my'];
const server = http.createServer((req, res) => {
  console.log('请求路径:', req.url);
  if(req.url === '/favicon.ico') { // 字体图标
    res.end('');
    return;
  }
  
  if(paths.includes(req.url)){  // 根路径
    res.end(fs.readFileSync(publicPath('index.html')))
    return;
  }

  if(req.url === '/js/index.js') {
    res.end(fs.readFileSync(publicPath('/js/index.js')))
    return;
  }

  if(req.url === '/list.html') {
    res.end(fs.readFileSync(publicPath('list.html')))
    return;
  }

  res.end('404');
});

server.listen('8002', () => {
  console.log('server port is 8001')
})
