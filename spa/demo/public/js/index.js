/*
 * @Author: Yh
 * @LastEditors: Yh
 */
class Router {
  constructor({routes, mode = 'hash'}){
    this.routes = routes;
    this.mode = mode;
    this.init();
  }
  init(){
    if(this.mode === 'history'){
      this.renderPage(window.location.pathname);
    }
    if(this.mode === 'hash'){
      const href = window.location.href;
      if(!href.includes('#')){
        window.location.href = '/#/';
      }
      this.renderPage(window.location.pathname);
    }
    this.stopA();
  }
  stopA(){
    const allATag = [...document.querySelectorAll('a')];
    // 所有的a标签。阻止a标签默认行为
    allATag.forEach((el) => {
      el.addEventListener('click', (e) => {
        e.preventDefault(); // 阻止默认行为
        // 跳转页面两种方式不通知服务器
        // hash地址后面加#号。服务器不会监听url#号后变化
        // history 通过window.history进行跳转
        this.jumpPage(el.getAttribute('href'));
      })
    })
  }
  jumpPage(path){
    if(this.mode === 'history') {
      // 页面跳转
      window.history.pushState(null, '', path);
    } else {
      window.location.href = '#'+path;
    }

    this.renderPage(path);
  }
  renderPage(path){
    const views = this.routes.find(val => val.path === path).component;
    document.getElementById('router-view').innerHTML = views;
  }
}


new Router({
  mode:'hash', // history
  routes: [
    {
      path: '/',
      component: `<div>首页</div>`
    },
    {
      path: '/type',
      component: `<div>分类</div>`
    },
    {
      path: '/my',
      component: `<div>我的</div>`
    }
  ]
});