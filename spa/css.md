<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
1. 实现元素水平垂直居中?

- 给父元素设置相对定位，子元素设置绝对定位，left:50%, top: 50%, transform: translate(-50%,-50%)

- 给父元素开启弹性盒设置align-items:center(垂直), justify-content:center(水平)

- 父元素设置相对定位，子元素设置绝对定位，top,left,right,bottom都为0；margin:auto

2. css盒模型，怎么修改盒模型

box model , 边距（margin） 边框（border） 填充（padding） 实际宽度（content-width）
通过 box-sizing属性来修改盒模型，默认值为cotent-box(width+padding+border)组成内容宽度  border-box（content-width= width - padding - border）

3. css 选择器的优先级， 怎么修改ui组件库样式，怎么实现组件内样式，vue style标签的scoped怎么实现的

- css 自上而下执行，就近读取
- 标签选择器 （div,p）
- id选择器（#app）
- 类选择器 （.class）
- 后代选择器 （div p）
- 子元素选择器 （div>p:last-child()）
- 伪类选择器 （a:hover,a:active）
- 伪元素 （a:after{content:"元素内容"}）

0. !important  -> style属性
1. id选择器
2. 类选择器
3. 标签
4. 子元素
5. 后代
6. 伪类


# 怎么修改ui库样式

利用层级选择器 找当前组件的根元素，`.container .antd-dialog {}`

# css module 通过 css-loader来实现


# 双飞翼布局 流式布局 多列布局

# rem px em 单位 ，怎么解决移动端适配问题，rem怎么计算根元素， postcss-pxtorem插件使用(计算css单位，自动讲css中px单位换算成rem单位)

# animation 是关键帧动画，不要事件触发。 transition是过渡动画，需要事件触发, 过渡元素属性变化的

# bfc 
块级格式化上下文规范
css独立渲染模式和区域
怎么形成bfc？
- overflow:hidden
- 浮动
- 定位
- display:inline-block/flex
