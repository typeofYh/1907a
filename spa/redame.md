<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
## spa

 单页面应用


- 思考：
  在浏览器url地址栏按回车会发生什么？
  1. 输入要访问地址`http://www.baidu.com`
  2. 通过`dns`(Domain Name Server，域名服务器), 得到ip地址, 数据传输的服务器地址
  3. 建立`tcp`链接
  4. 发起三次握手
  5. 发送`http`请求,进行数据传输
  6. 服务器根据请求类型，响应不同结果给到浏览器，浏览器进行数据处理渲染到页面
  7. 服务器进行响应之后自动断开链接
  8. 进行四次挥手 ?? 
  9. 断开`tcp`链接

- 浏览器怎么渲染页面？
1. 解析html文旦获取dom树   html => head => body => display:none的元素也会重新进行dom计算，style标签 script标签都属于html dom，解析范围 js和css有可能组件浏览器解析
2. 处理css 计算 css模型  
3. 将css和dom结合计算dom元素位置
4. 讲结合之后元素渲染生成layout布局
5. 将layout绘制到浏览器窗口


回流（重排） 、 重绘

## http

http是超文本传输协议，主要针对浏览器和服务器进行通信的。
1. 必须浏览器主动向服务器发送请求
2. 要携带合法的headers和body，请求头和报文
3. 服务器向浏览器响应请求结果
4. 浏览器得到服务的响应结果自动断开链接


## 在浏览器中哪些会发起http请求
1. url地址按回车 get形式请求
2. 图片标签的src、script标签的src、iframe标签的src、a标签的href、link标签的href属性、audio、video标签的src
html结构中发起的请求

3. ajax和fetch
 ajax是一种技术，用于实现在不加载整个页面的情况下，实现局部数据更新的技术，用来向服务发起http请求，

 fetch是浏览器封装好的发起http请求的方法，返回值是一个promise

 ajax和fetch受同源策略限制会有跨域问题

4. 服务器和服务器之间可以进行通信，不受同源策略限制

## 如何解决跨域问题
1. 开发环境解决跨域问题
 - 开发环境的服务器是webpack的devServer启动的，通过devServer的proxy代理来实现跨域

2. 正式环境
 - 通过webpack打包文件。得到html、js、css
 - 要把html、js、css放到服务器上，通过nginx（web服务器软件）来替代webpack的devServer
 - 修改nginx配置，配置proxy_pass字段解决跨域问题


 - jsonp 通过script src属性来解决跨域问题，只能发起get请求，而且接口需要单独定义，通过回调函数的形式进行数据传递
  script 标签的src属性会发起http请求，只要是合法的js脚本就会解析运行，动态创建script标签，添加到页面，通过get方式请求向后端发送参数，后端需要单独定义接口返回值，以对调函数的形式传递数据

 - cors 跨域资源共享，主要在后端设置,后端在返回数据的时候告诉浏览器xx地址请求的我正常返回 

 ```js
 Access-Control-Allow-Origin
 ```

 - postmessage 主要用于主窗口和子窗口进行通信




