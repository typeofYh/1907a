<!--
 * @Author: Yh
 * @LastEditors: Yh
-->



## web层面优化

1. 浏览器缓存，优化静态资源文件加载速度

- 强缓存

response headers (响应头) 服务端响应资源回来的设置的headers头
cache-control： http2.0
expires： http1.0

disk cache (第一次打开页面走磁盘缓存) / memory cache（第二次加载页面走记忆缓存）

- 协商缓存

response headers (响应头)

> etag 文件版本  If-None-Match   浏览器向服务器发请求，服务器返回资源并且设置etag头，强缓存失效再次向服务器发请求（这时浏览器会自动携带If-None-Match头过来）

> last-modified 文件最后一次修改时间

---

2. http传输压缩，gzip

3. 利用cdn分布式网络