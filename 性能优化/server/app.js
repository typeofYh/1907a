/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const http = require('http');
const fs = require('fs');
const path = require('path');

const publicPath = (files) => path.join(__dirname,'./public',files);

const etag = 1;

const server = http.createServer((req, res) => {
  console.log('request-url:', req.url, Math.round(Date.now() / 1000));
  if(req.url === '/favicon.ico'){  // 网站图标
    res.end('');
    return;
  }
  if(req.url === '/'){
    res.end(fs.readFileSync(publicPath('index.html')));  // 根路径页面
    return;
  }

  if(req.url === '/js/index.js'){
    // console.log(re)
    res.setHeader('cache-control', `max-age=${10}`)  //缓存规则  max-age是相对时间相对第一次请求的时间 http2.0
    // res.setHeader('expires', new Date('2022/5/25').toGMTString()) // http1.0
    // 取到上一次返回给浏览器的etag
    const IfNoneMatch = req.headers['If-None-Match'];
    if(IfNoneMatch === etag){  // 文件没有发生变化
      res.statusCode = 304;
      res.end();  //结束响应
      return;
    }
    res.setHeader('etag', etag);
    res.statusCode = 200;
    res.end(fs.readFileSync(publicPath('/js/index.js')));  // 结果
    return;
  }
})

server.listen(3000, () => {
  console.log('prot is 3000');
})